const express = require('express')
const mongoose = require('mongoose')
const path = require('path')
const cors = require('cors')
const bodyParser = require('body-parser');

const app = express()
app.use(cors())

const server = require('http').Server(app)
const io = require('socket.io')(server)

io.on('connection', socket => {
    console.log('IO connectado!')

    socket.on('connectRoom', box => {
        socket.join(box)
    })
})

mongoose.connect('mongodb+srv://goweek:goweek@goweek-gkfrp.mongodb.net/test?retryWrites=true', { useNewUrlParser: true })

app.use((req, res, next) => {
    req.io = io;
    return next();
})

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }));

app.use('/files', express.static(path.resolve(__dirname, '..', 'tmp')))

app.use(require('./routes.js'))

server.listen(process.env.PORT || 3333, (err) => {
    if (err) {
        console.log('Erro no servidor...')
    } else {
        console.log('Rodando em 3333...')
    }
})